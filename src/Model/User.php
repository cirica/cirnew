<?php

namespace Model;

class User
{
    CONST TABLE_NAME = 'user';

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $username;

    /** @var string */
    public $gender;

    /** @var Roles[] */
    public $roles = [];

    /**
     * @return Roles[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param Roles[] $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        echo 'xxx';
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }
}