<?php

namespace Libraries;

interface RepositoryInterface
{
    function add($object);
    function remove($object);
    function update($object);
    function delete($object);
    function find($id);
    function findAll();
}