<?php

namespace Libraries;

class Request
{
    public $query = [];

    public $request = [];

    public function __construct()
    {
    }

    public function initialize($get, $request)
    {
        if(isset($get)){
            $this->query = $get;
        }

        if(isset($request)){
            $this->request = $request;
        }
    }

    /**
     * @return array
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @param array $query
     */
    public function setQuery(array $query): void
    {
        $this->query = $query;
    }

    /**
     * @return array
     */
    public function getRequest(): array
    {
        return $this->request;
    }

    /**
     * @param array $request
     */
    public function setRequest(array $request): void
    {
        $this->request = $request;
    }

}