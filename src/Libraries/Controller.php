<?php

namespace Libraries;

abstract class Controller
{
    /** @var Request  */
    public $request;

    public $twig;

//    /** @var Repository */
//    public $repository;


    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->twig = Twig::build();

    }

    public function render($file, $data){
        try{


        $template = $this->twig->load($file);
        echo $template->render($data);
        } catch (\Exception $exception)
        {
            echo $exception->getMessage();
        }
    }

    public function getRepository($model)
    {
        return Repository::build($model);
    }
//
//    /**
//     * @param Repository $repository
//     */
//    public function setRepository(Repository $repository): void
//    {
//        $this->repository = $repository;
//    }

}