<?php

namespace Libraries;

class Core
{
    public $controller = 'Controller\\DefaultController';
    public $method = 'index';
    public $params = [];


    public function __construct()
    {

        var_dump($_GET);

        if (isset($_GET) && !empty($_GET)) {
            $url = $_GET['url'];
        }

        if (isset($url)) {
            $url = explode('/', $url);
        }


        $controllerInstance = $this->controller;
        if (!empty($url[0])) {
            $this->controller = "Controller\\" . ucwords($url[0]) . 'Controller';
            if (class_exists($this->controller)) {
//                $controllerInstance = new $this->controller;
                unset($url[0]);
            }
        }

        if (isset($url[1])) {
            $this->method = $url[1];
//            if (method_exists($this->controller, $this->method)) {
            if (in_array($this->method, get_class_methods($this->controller))) {
                unset($url[1]);
            }
        }

        if (!empty($url)) {
            $this->params = array_values($url);
        }

        $request = $this->createRequest(array_values($url), $_POST);

        $controllerInstance = new $this->controller($request);

        call_user_func([$controllerInstance, $this->method], $this->params);
    }

    protected function createRequest($get, $post)
    {
        $request = new Request();
        $request->initialize($get, $post);

        return $request;
    }
}