<?php

namespace Libraries;

use Libraries\RepositoryInterface;

abstract class Repository implements RepositoryInterface
{
    private $database;

    private static $model;

    const TABLE_NAME = '';

    public function __construct()
    {
        $this->database = Database::build();
    }

    public static function build($model)
    {
        self::$model = $model;
        $class = 'Repository\\' . $model . 'Repository';
        if (class_exists($class)) {
            return new $class;
        }
    }

    function add($object)
    {
        $properties = get_object_vars($object);

        $fields = implode('\',\'', array_keys($properties));

        $values = implode('\',\'', array_values($properties));

        echo $sql = 'Insert into ' . self::$model::TABLE_NAME . ' (' . $fields . ') values ( ' . $values . ')';


    }

    function remove($object)
    {
        echo $sql = 'delete from ' . $object::TABLE_NAME . ' where id =' . $object->getId();
        // TODO: Implement remove() method.
    }

    function update($object)
    {
        $properties = get_object_vars($object);

        $concat = [];
        foreach ($properties as $key => $value) {
            $concat[] = $key . '= \'' . $value . '\'';

        }

        echo get_called_class();

        echo $sql = 'update ' . $object::TABLE_NAME . ' set ' . implode(',', $concat) . ' where id=' . $object->getId();


        // TODO: Implement update() method.
    }

    function delete($object)
    {
        // TODO: Implement delete() method.
    }

    function find($id)
    {
//        $calledClass = get_called_class();
//        $repositoryName = str_replace('Repository\\', '', $calledClass);
//
//        $objectName = 'Model\\' . str_replace('Repository', '', $repositoryName);
        $objectName = 'Model\\'. self::$model;
        $object = new $objectName;

        $sql = 'Select * from ' . $object::TABLE_NAME . ' where id= :id';
        $connection = $this->database->prepare($sql);
        $connection->fetchObject($objectName);
        $connection->execute([':id' => $id]);


        $results = $connection->fetch();

        return $results;

        return $this->createObject($results, new $objectName);


        // TODO: Implement find() method.
    }

    function join()
    {
        $object = $this->getModel();

        $sql = 'Select * from ' . $object::TABLE_NAME . ' a 
        inner join roles r on a.role_id = r.id
        where a.id= :id';
    }

//    public function getModel()
//    {
//        $calledClass = get_called_class();
//        $repositoryName = str_replace('Repository\\','', $calledClass);
//
//        $objectName = 'Model\\' . str_replace('Repository', '',$repositoryName);
//
//        $object = new $objectName;
//
//        return $object;
//    }

    public function createObject($data, $object)
    {


        $fields = array_keys(get_object_vars($object));

        var_dump(get_object_vars($object));
        die;
        foreach ($fields as $field) {
            echo $field;

            $methodName = 'set' . ucfirst($field);
            if (method_exists($object, $methodName)) {
                call_user_func([$object, $methodName], $data[$field]);
            } else {
                throw new \Exception("cannot set Property for field: " . $field);
            }
        }

        return $object;
    }

    function findAll()
    {

    }
}