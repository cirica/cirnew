<?php

namespace Libraries;

class Database
{
    private static $instance = null;

    public static function build(){

        if (is_null(self::$instance)) {
            try {
                $db = 'mysql:host=localhost;dbname=test';
                $user = 'root';
                $pass = 'Cristi`12';

                self::$instance = new \PDO($db, $user, $pass);
            } catch (\PDOException $exception) {
                throw new \Exception($exception->getMessage());
            }
        }

        return self::$instance;

    }
}