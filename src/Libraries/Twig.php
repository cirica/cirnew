<?php

namespace Libraries;

class Twig
{
    private static $instance = null;

    public static function build()
    {
        if(is_null(self::$instance))
        {
            $loader = new \Twig\Loader\FilesystemLoader(__DIR__.'/../../templates');
            $twig = new \Twig\Environment($loader, ['debug' => true]


//                [
//                'cache' => __DIR__.'./../../cache',
//            ]
            );

            self::$instance = $twig;
        }
        return self::$instance;
    }

}